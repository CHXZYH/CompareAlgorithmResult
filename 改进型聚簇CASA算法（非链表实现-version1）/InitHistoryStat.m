function InitHistoryStat(Interval_num)
global history;
history.tau=-ones(1,Interval_num);
%CurrStat related data
history.ReadHitCount=-ones(1,Interval_num);
history.ReadMissCount=-ones(1,Interval_num);
history.WriteHitCount=-ones(1,Interval_num);
history.WriteMissCount=-ones(1,Interval_num);
history.TotalCount=-ones(1,Interval_num);
history.AveDelay=-ones(1,Interval_num);
%end
history.CLRU_length=-ones(1,Interval_num);
history.DLRU_length=-ones(1,Interval_num);
end