function Index=FindCLRUMinAge()
% 利用逻辑运算提高运算速度
    global CLRU;
    global NandPageArr;
% 和NandPage.Cache_Stat有关的宏定义（在不在缓冲区,在哪个队列）
    global Cache_Invalid;
    global Cache_InCLRU;
    global Cache_InDLRU;
%   必须先把数据提出来
    temp=NandPageArr(:).Cache_Age;
    MaxAge=max(temp);
%   CLRU中找最小，排除不是CLRU队列中数据
    MatchArr=(NandPageArr.Cache_Stat==Cache_InCLRU);
    MatchArr=~MatchArr;
%   将无关的数据项的age都设置为当前最大的Cache_Age
    MatchArr=MatchArr.*MaxAge;
%   将其相关的数据项叠加，选择最小的数据项
    MatchArr=MatchArr+NandPageArr.Cache_Age;
    [MinAge,MinLPN]=min(MatchArr);
%   此时返回的index是最小的LPN号
    tempArr=(CLRU.list==MinLPN);
    [V,Index]=max(tempArr);
end