function Index=FindValueInArr(Arr,Value)
% 该函数主要实现匹配Arr中非0值，等于value值的位置索引Index
% 如果找不到对应的Value值，则返回0
    Index=0;
    L=length(Arr);
    NL=sum(Arr>0);
    for i=1:L
        if NL<=0
            break;
        end
%      数组中的非零项中检索是否存在该Value
        if Arr(i)>0
            if Arr(i)==Value
                Index=i;
                break;
            end
            NL=NL-1;
        end
    end
    return ;
end