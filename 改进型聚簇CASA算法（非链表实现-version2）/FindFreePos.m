function Index=FindFreePos(Arr)
% 找到数组中空闲的位置（Index），该位置的数据是0返回
% 如果找不到空闲的位置，返回0
global buf_size;
    L=length(Arr);
    if L>buf_size
        error('the Arr-size over limit\t please to find debug\n');
    end
    Index=0;
    for i=1:L
        if Arr(i)==0
            Index=i;
            break;
        end
    end
    return;
end
