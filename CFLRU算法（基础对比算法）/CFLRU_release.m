%CFLRU_release.mat
clc
clear all;
trace_filename='..\..\test_trace\Syn1-5\syn1.txt';%配置输入的trace文件
output_filename='.\result\syn1_CFLRU_result_buffsize_4096_alpha0.4_var.mat';%设置输出的指定文件
[req_arrive,device_num,req_sec_start,req_sec_size,req_type]=textread(trace_filename,'%f%d%d%d%d');
%读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型(0表示写请求，1读请求)）
%设置缓冲区的大小
buf_size=4096;
Alpha=0.4;%剔除窗口占buff的比例
w=Alpha*buf_size;%剔除窗口的大小S
% 缓冲区存的数为-1表示该位置是空闲的，buf的第二行是存请求的读写位，存的是-1表示未使用
%buf的第二行也可以作为脏页的flag，如果存的是1则表示为脏页，存的是0表示为干净页
buf=-ones(2,buf_size);
%输出参量的初始化
miss_cnt=0;
evict_cnt=0;
hit_rate=0;
write_hit_count=0;%写请求命中次数
read_hit_count=0;%读请求命中次数
write_miss_count=0;
read_miss_count=0;
write_back_count=0;
physical_read_count=0;
physical_write_count=0;
all_page_req_num=0;%统计所有的页请求次数
% 关于读写代价的设置和写放大系数的调整
global FlashParameter;
FlashParameter.rCost=50;
FlashParameter.wCost=250;
FlashParameter.wAmp =1.35;

%对请求扇区序列进行次数统计,有多少行就是有多少次请求
all_req_sec_num=length(req_sec_start);
% 观察周期Interval
Interval=10000;
% 观测保留的中间值
StatLast.read_hit_count=0;
StatLast.read_miss_count=0;
StatLast.write_miss_count=0;
StatLast.write_hit_count=0;
StatLast.all_page_req_num=0;

%关于页对齐的参数
SECT_NUM_PER_PAGE=4; %每个页包含多少个扇区
req_LPN_start=0;%req_LPN_start=req_sec_start/SECT_NUM_PER_PAGE
req_page_size=0;%请求的页大小,req_page_size=(req_sec_start+req_sec_size-1)/SECT_NUM_PER_PAGE-req_sec_start/SECT_NUM_PER_PAGE+1
tic
%开始处理请求，进行循环
for req_index=1:all_req_sec_num
%request_index表示当前请求在总请求序列中的次序
 %对扇区请求页对齐
    req_LPN_start=fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE);
    req_page_size=fix((req_sec_start(req_index)+req_sec_size(req_index)-1)/SECT_NUM_PER_PAGE)-fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE)+1;
    req_LPN_type=req_type(req_index);%当前的读写请求类型
    all_page_req_num=all_page_req_num+req_page_size;%统计所有的页请求次数
    
%   统计和输出观察周期内的统计数据
    if mod(req_index,Interval)==0
        fprintf('req=%d, curr-buff-length is %d\n',req_index,sum(buf(1,:)>0));        
        CurrStat.ReadHitCount=read_hit_count-StatLast.read_hit_count;
        CurrStat.ReadMissCount=read_miss_count-StatLast.read_miss_count;
        CurrStat.WriteHitCount=write_hit_count-StatLast.write_hit_count;
        CurrStat.WriteMissCount=write_miss_count-StatLast.write_miss_count;
        %Interval期间内发生了多少次的页请求
        CurrStat.TotalCount    =all_page_req_num-StatLast.all_page_req_num;
        CurrStat.AveDelay=(double(CurrStat.ReadMissCount*FlashParameter.rCost+CurrStat.WriteMissCount*FlashParameter.wCost*FlashParameter.wAmp)/CurrStat.TotalCount);
        DisplayCurrStat(CurrStat);
        StatLast.read_hit_count=read_hit_count;
        StatLast.read_miss_count=read_miss_count;
        StatLast.write_hit_count=write_hit_count;
        StatLast.write_miss_count=write_miss_count;
        StatLast.all_page_req_num=all_page_req_num;
    end
% --------------------------------------------------------------    
    while(req_page_size>0)
        %遍历buf 查看是否存有该LPN号,返回flag=1则命中
        flag=0;
        for buf_index=1:buf_size
        %这里修改代码注意req_list的第一列存的是LPN号，第二列存的是脏页flag
        %命中缓冲区
            if buf(1,buf_index)==req_LPN_start
            %如果命中了还得查看命中的是什么请求：
            %如果是写请求，命中的原来的buf中干净页，则把对应的flag置1，
            %如果是读请求，则保留命中原来的buf中的flag设置不变。
                if req_LPN_type==0
                    buf(2,buf_index)=1;%服务写请求，该页变为脏页
                    write_hit_count=write_hit_count+1;%命中写请求次数统计
                else
                    %命中的是读请求，则对命中的buf项的flag不做修改
                    read_hit_count=read_hit_count+1;%命中读请求次数统计
                end         
                %依据LRU策略，将命中的页存到队列的头部
                buf=[buf(:,buf_index),buf(:,1:buf_index-1),buf(:,buf_index+1:end)];
                flag=1;
                break;
            end
        end
        %若flag=0则表示未命中
        if flag==0
            %没有命中缓冲区需要判断两种情况缓冲区有没有满，满了要剔除满不满的问题，其实再数组赋值的时候就把最后项就移出去了
            %这里添加新的东西，记录下未命中的是什么请求
            physical_read_count=physical_read_count+1;
            if req_LPN_type==0
                write_miss_count=write_miss_count+1;
                buf_temp=[req_LPN_start,1]';
            else
                read_miss_count=read_miss_count+1;
                buf_temp=[req_LPN_start,0]';
            end
            %如果要考虑满了剔除操作的问题，CFLRU这就要w窗口内优先剔除干净页，剔除干净页是不需要记录回写次数的
            %如果没有干净页，则依据LRU策略剔除脏页，则需要记录回写次数
            %先考虑剔除操作
            if buf(1,end)~=-1
                %如果buf最后尾部存有不等于-1的数，LPN号都为正整数，则认为队列满，则需要剔除
                evict_cnt=evict_cnt+1;%剔除次数累加
                %从尾部开始遍历w窗口区的页的flag，找到第一个干净页剔除，如果没有则直接按按LRU策略剔除尾项
                w_flag=0;%w_flag剔除过程中检测到干净页，则置1
                for w_index=buf_size:-1:buf_size-w+1
                    if buf(2,w_index)==0
                        w_flag=1;
                        break;
                    end
                end
                
                if w_flag==1    %w_flag=1，表示检测到有干净页，则替换该项
                    buf=[buf_temp,buf(:,1:w_index-1),buf(:,w_index+1:end)];
                else
                %w_flag==0表示没有检测到干净页，则按LRU策略直接剔除尾项
                %根据LRU策略把刚加载的数据放到队列的头部,同时统计脏页的回写次数
                    buf=[buf_temp,buf(:,1:end-1)];
                    write_back_count=write_back_count+1;
                    physical_write_count=physical_write_count+1;
                end
            else%如果缓冲区没有满，则将缺的项加载到队列的头部（简单的LRU加载）
                buf=[buf_temp,buf(:,1:end-1)];
            end
                miss_cnt=miss_cnt+1;%请求未命中次数统计
        end
     req_page_size=req_page_size-1;%请求页完成一次，大小减一
     req_LPN_start=req_LPN_start+1;
    end%endwhile
end%end_for1
toc
%计算命中率
hit_rate=double((all_page_req_num-miss_cnt))/all_page_req_num;
%计算写命中率
write_hit_rate=double(write_hit_count)/(write_hit_count+write_miss_count);
%计算读命中率
read_hit_rate=double(read_hit_count)/(read_hit_count+read_miss_count);
%输出参数
fprintf('the all_page_req_num is %d\n',all_page_req_num);
fprintf('the hit rate is %10.8f\n',hit_rate);
fprintf('the evict number is %d\n',evict_cnt);
fprintf('the  all_write_count is %d\n', write_miss_count+write_hit_count);
fprintf('the  write_hit_count is %d\n', write_hit_count);
fprintf('the write_hit_rate is %10.8f\n',write_hit_rate);
fprintf('the  all_read_count is %d\n', read_miss_count+read_hit_count);
fprintf('the read_hit_count is %d\n',read_hit_count);
fprintf('the read_hit_rate is %10.8f\n',read_hit_rate);
fprintf('the physical_write_count is %d\n',physical_write_count);
fprintf('the estimated average response time is %7.4f\n',(double(read_miss_count*FlashParameter.rCost+write_miss_count*FlashParameter.wCost*FlashParameter.wAmp)/all_page_req_num));
%保存结果到指定文件
clear req_arrive device_num req_sec_start req_sec_size req_type ;
clear buf buf_temp buf_index;
save(output_filename);