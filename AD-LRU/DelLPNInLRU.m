function [LRU]=DelLPNInLRU(LRU)
% 删除对应的LRU队列中的数据项，同时记录统计信息(删除脏页--物理回写次数)
    global Stat;
%  首先判断是否存在干净页候选项
    if LRU.FC~=0
        DelIndex=LRU.FC;
        LRU.list=[LRU.list(:,1:DelIndex-1),LRU.list(:,DelIndex+1:end)];
        LRU.length=LRU.length-1;
%       注意重新更新对应的FC
        [LRU]=UpdateLRUFC(LRU);
    else
        [LRU]=FindDrityVictimInLRU(LRU);
%       删除LRU位置的脏页
        LRU.list=LRU.list(:,1:end-1);
        LRU.length=LRU.length-1;
%       统计信息记录
        Stat.physical_write_count=Stat.physical_write_count+1;
        Stat.write_back_count=Stat.write_back_count+1;
    end
end