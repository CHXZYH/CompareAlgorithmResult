function [ColdLRU,HotLRU]=ColdToHot(Index,req_type,ColdLRU,HotLRU)
%     该函数负责将ColdLRU再次命中的数据项移动到HotLRU中的MRU位置,该函数同时完成对应的FC更新
       temp=ColdLRU.list(:,Index);
%      如果命中的是写请求需要修改脏页标识符号
       if req_type==0
            temp(2)=1;
       end
%      重新置位访问位1
       temp(3)=1;
%      剔除ColdLRU中的数据项
       ColdLRU.list=[ColdLRU.list(:,1:Index-1),ColdLRU.list(:,Index+1:end)];
       ColdLRU.length=ColdLRU.length-1;
%      注意更新对应的FC指针
       [ColdLRU]=UpdateLRUFC(ColdLRU);
%      将temp嵌入到HotLRU
        HotLRU.list=[temp,HotLRU.list(:,1:end)];
        HotLRU.length=HotLRU.length+1;
%      注意更新对应的FC指针
        [HotLRU]=UpdateLRUFC(HotLRU);
        
end