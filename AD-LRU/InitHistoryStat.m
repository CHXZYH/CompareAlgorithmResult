function InitHistoryStat(Interval_num)
global history;
% history.tau=-ones(1,Interval_num);
%CurrStat related data
history.ReadHitCount=zeros(1,Interval_num);
history.ReadMissCount=zeros(1,Interval_num);
history.WriteHitCount=zeros(1,Interval_num);
history.WriteMissCount=zeros(1,Interval_num);
history.TotalCount=zeros(1,Interval_num);
history.AveDelay=zeros(1,Interval_num);
%end
% 添加其他相关算法的统计信息
history.HotLRU_length=zeros(1,Interval_num);
history.ColdLRU_length=zeros(1,Interval_num);
end