%%AD_LRU_release
clc
clear all;
trace_filename='.\test_trace\financial1';%配置输入的trace文件
output_filename='.\result\ADLRU\trace_financial1_ADLRU_result_buffsize_8000_var.mat';%设置输出的指定文件
[req_arrive,device_num,req_sec_start,req_sec_size,req_type]=textread(trace_filename,'%f%d%d%d%d');
%读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型(0表示写请求，1读请求)）

%设置缓冲区的大小
buf_size=8000;
alpha=0.4;
% 缓冲区存的数为-1表示该位置是空闲的，buf的第二行是存请求的读写位，存的是-1表示未使用
%buf的第二行也可以作为脏页的flag，如果存的是1则表示为脏页，存的是0表示为干净页,第三行是二次机会标识位，引用置1，扫描到置0
Lh=[];%作为热数据队列，存放至少访问两次的数据
Lc=[];%作为冷数队列，存放只访问一次的数据
%输出参量的初始化
min_Lc=buf_size*alpha;%缓冲区满时，Lc最小区间
h=0;%热数据队列的大小
c=0;%冷数队列的大小
%h+c<=buf_size
C_FC=0;%指向冷数队列的最近最少使用的干净页的索引
H_FC=0;%指向热数据队列中最近最少使用的干净页的索引
ADLRU_miss_cnt=0;
ADLRU_evict_cnt=0;
ADLRU_hit_cnt=0;
ADLRU_write_hit_count=0;%写请求命中次数
ADLRU_read_hit_count=0;%读请求命中次数
ADLRU_write_miss_count=0;
ADLRU_read_miss_count=0;
ADLRU_write_back_count=0;
all_page_req_num=0;%统计所有的页请求次数

%对请求扇区序列进行次数统计,有多少行就是有多少次请求
all_req_sec_num=length(req_sec_start);
%关于页对齐的参数
SECT_NUM_PER_PAGE=4; %每个页包含多少个扇区
req_LPN_start=0;%req_LPN_start=req_sec_start/SECT_NUM_PER_PAGE
req_page_size=0;%请求的页大小,req_page_size=(req_sec_start+req_sec_size-1)/SECT_NUM_PER_PAGE-req_sec_start/SECT_NUM_PER_PAGE+1
buf_current_size=0;%当前buf的大小

%%开始处理请求，进行循环
for req_index=1:all_req_sec_num
%request_index表示当前请求在总请求序列中的次序
 %对扇区请求页对齐
    req_LPN_start=fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE);
    req_page_size=fix((req_sec_start(req_index)+req_sec_size(req_index)-1)/SECT_NUM_PER_PAGE)-fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE)+1;
    req_LPN_type=req_type(req_index);%当前的读写请求类型
    all_page_req_num=all_page_req_num+req_page_size;%统计所有的页请求次数
    while(req_page_size>0)
        %遍历buf 查看是否存有该LPN号,返回flag=1则命中
        flag=0;
        %%添加算法代码段
        %先查看请求页是否在Lh中
        for Lh_index=1:h
            if Lh(1,Lh_index)==req_LPN_start%命中请求
                ADLRU_hit_cnt=ADLRU_hit_cnt+1;
                if req_LPN_type==0%如果是写请求
                    ADLRU_write_hit_count=ADLRU_write_hit_count+1;
                    Lh(2,Lh_index)=1;%脏页标识置位
                else
                    ADLRU_read_hit_count=ADLRU_read_hit_count+1;
                end
                Lh(3,Lh_index)=1;%引用位置1
                %将命中项移到队列的MRU位置
                Lh=[Lh(:,Lh_index),Lh(:,1:Lh_index-1),Lh(:,Lh_index+1:end)];
                %调整当前队列的FC指向
%               上述的数组移动可能带动H_FC指向的目标右移
                if H_FC<h%放置H_FC的越界
                    H_FC=H_FC+1;
                end
%               从当前假定的H_FC位置向前检索干净页
                for i=H_FC:-1:0
                    if i==0
                        break;
                    end
                    if Lh(2,i)==0%检索到干净页
                        break;
                    end
                end
                H_FC=i;%如果i=0，表示当前队列没有干净页了
                flag=1;
                break;
            end%Lh命中请求完成
        end
        if flag==0%表示没有命中Lh队列，遍历Lc队列
            for Lc_index=1:c
                if Lc(1,Lc_index)==req_LPN_start%命中Lc冷区的数据页
                    ADLRU_hit_cnt=ADLRU_hit_cnt+1;
                    if req_LPN_type==0%如果是写请求
                        ADLRU_write_hit_count=ADLRU_write_hit_count+1;
                        Lc(2,Lc_index)=1;%脏页标识置位
                    else
                        ADLRU_read_hit_count=ADLRU_read_hit_count+1;
                    end
                    Lc(3,Lc_index)=1;%引用位置1
                    %将命中的p移动到Lh的MRU位置
                    Lh=[Lc(:,Lc_index),Lh(:,1:end)];
                    h=h+1;
%                   调整Lh的H_FC指向
%                   如果H_FC=0表示之前的Lh没有干净页
                    if H_FC==0
                        if Lc(2,Lc_index)==0%表示插入的就是干净页,就指向插入的干净页
                            H_FC=1;
                        end
                    else%之前的Lh中存在干净页，头部插入数据导致H_FC变化加一
                        H_FC=H_FC+1;
                    end
%                     将命中的p移除Lc
                    Lc=[Lc(:,1:Lc_index-1),Lc(:,Lc_index+1:end)];
                    c=c-1;
%                   调整Lc的C_FC指项
                    if c>0%表示Lc为非空可以检索干净页
%                      从之前的C_FC向前检索干净页
%                      防止删除的刚好就是CF_C导致越界，需要预加判断
                        if C_FC>c
                            C_FC=C_FC-1;
                        end
                        for j=C_FC:-1:0
                            if j==0
                                break;
                            end
                            if Lc(2,j)==0%检索到干净页
                                break;
                            end
                        end
                        C_FC=j;
                    else%要考虑下Lc可能为空的特殊情况
                        C_FC=0;%当Lc为空时，C_CF肯定为0
                    end
%                     调整C_FC完毕
                    flag=1;
                    break;
                end%Lc命中请求完成
            end%遍历Lc操作完成
        end
        %表示没有命中缓冲区
        if flag==0
            if(c+h==buf_size)%如果缓冲区满了，考虑剔除操作
                if c>min_Lc%Lc还可以剔除
                    if C_FC~=0  %存在干净页，优先剔除干净页
                        Lc=[Lc(:,1:C_FC-1),Lc(:,C_FC+1:end)];%剔除此干净页
                        c=c-1;
                        %调整FC的指向，从之前的C_FC开始寻找下一个干净页
                        if C_FC>c
                            C_FC=C_FC-1;
                        end
                        for j=C_FC:-1:0
                            if j==0
                                break;
                            end
                            if Lc(2,j)==0%检索到干净页
                                break;
                            end
                        end
                        C_FC=j;
                    else%(Fc为空，此时考虑剔除脏页,从尾部LRU开始剔除)
                        while(Lc(3,end)==1)
                            Lc(3,end)=0;%LRU检索到引用项为1，置0，放到MRU位置，检索下一项
                            Lc=[Lc(:,end),Lc(:,1:end-1)];
                        end
                        %通过while，确定当前的LRU的victim是剔除项
                        Lc=Lc(:,1:end-1);
                        ADLRU_write_back_count=ADLRU_write_back_count+1;%记录脏页回写次数
                        c=c-1;
                    end%Lc剔除操作完成
                else%(考虑剔除Lh的数据页)
                    if H_FC~=0 %存在干净页优先剔除干净页
                        Lh=[Lh(:,1:H_FC-1),Lh(:,H_FC+1:end)];%剔除此干净页
                        h=h-1;
                        %调整FC的指向，从之前的H_FC开始寻找下一个干净页
                        if H_FC>h
                            H_FC=H_FC-1;
                        end
                        for j=H_FC:-1:0
                            if j==0
                                break;
                            end
                            if Lh(2,j)==0%检索到干净页
                                break;
                            end
                        end
                        H_FC=j;
                    else%(Fc为空，此时考虑剔除脏页,从尾部LRU开始剔除)
                        while(Lh(3,end)==1)
                            Lh(3,end)=0;%将检索到的LRU置0
                            Lh=[Lh(:,end),Lh(:,1:end-1)];
                        end
                        %通过while，确定当前的LRU的victim是剔除项
                        Lh=Lh(:,1:end-1);
                        ADLRU_write_back_count=ADLRU_write_back_count+1;%记录脏页回写次数
                        h=h-1;
                    end%Lh剔除操作完成
                end
                ADLRU_evict_cnt=ADLRU_evict_cnt+1;
            end%剔除操作完成
            %将第一次访问的数据放到LC的MRU位置，访问位置1
            if req_LPN_type==0 %写请求未命中
                ADLRU_write_miss_count=ADLRU_write_miss_count+1;
                buf_tmp=[req_LPN_start,1,1]';
            else
                ADLRU_read_miss_count=ADLRU_read_miss_count+1;
                buf_tmp=[req_LPN_start,0,1]';
            end
            Lc=[buf_tmp,Lc(:,1:end)];%将第一次访问的数据放到LC的MRU位置
            c=c+1;
            %调整Lc中的C_FC
            if C_FC==0%表示没有干净页
                if buf_tmp(2)==0%加入的是干净页
                    C_FC=1;
                end
            else%有干净页
                C_FC=C_FC+1;%加入数据导致移位
            end
           ADLRU_miss_cnt=ADLRU_miss_cnt+1; 
        end%缓冲区未命中操作完成
    req_page_size=req_page_size-1;%请求页完成一次，大小减一
    req_LPN_start=req_LPN_start+1;  
    end%end-一次页请求操作
end%end-所有req请求操作
%计算命中率
ADLRU_hit_rate=double((all_page_req_num-ADLRU_miss_cnt))/all_page_req_num;
%计算写命中率
ADLRU_write_hit_rate=double(ADLRU_write_hit_count)/(ADLRU_write_hit_count+ADLRU_write_miss_count);
%计算读命中率
ADLRU_read_hit_rate=double(ADLRU_read_hit_count)/(ADLRU_read_hit_count+ADLRU_read_miss_count);
%输出参数
fprintf('the all_page_req_num is %d\n',all_page_req_num);
fprintf('the AD-LRU hit rate is %10.8f\n',ADLRU_hit_rate);
fprintf('the AD-LRU evict number is %d\n',ADLRU_evict_cnt);
fprintf('the  all_write_count is %d\n', ADLRU_write_miss_count+ADLRU_write_hit_count);
fprintf('the  AD-LRU write_hit_count is %d\n', ADLRU_write_hit_count);
fprintf('the AD-LRU write_hit_rate is %10.8f\n',ADLRU_write_hit_rate);
fprintf('the  AD-LRU all_read_count is %d\n', ADLRU_read_miss_count+ADLRU_read_hit_count);
fprintf('the AD-LRU read_hit_count is %d\n',ADLRU_read_hit_count);
fprintf('the AD-LRU read_hit_rate is %10.8f\n',ADLRU_read_hit_rate);
%保存结果到指定文件
clear req_arrive device_num req_sec_start req_sec_size req_type ;
clear Lc Lh;
save(output_filename);